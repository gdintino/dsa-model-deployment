# UTN FRC - Diplomatura en Data Science Aplicada

## Despligue de un modelo de ciencia de datos.

### Objetivo

Este repositorio contiene un ejemplo simple de como desplegar un modelo de ciencia de datos basado en un Jupyter Notebook utilizando tres diferentes modalidades:

- Convirtiendo en código el modelo basado en el notebook.
- Implementando una interfaz de línea de comandos para entrenar, entrenar y predecir o predecir en base al modelo definido en el notebook.
- Publicar una Web API (ReST) la cual expone diferentes endpoints para interactuar con el modelo.

### Descripción de la solución

**Repositorio:** [dsa-model-deployment](https://gitlab.com/gdintino/dsa-model-deployment)

**Tecnología:** Python 3.x

**Estructura del proyecto:**

![Estructura del proyecto](./resources/project_structure.png)

**Descripción del contenido:**

- el directorio **dsa-model-deployment** contiene la implementación de la solución. Se destacan el archivo *README.md* y *requirements.txt*. El primero contiene esta descripción que se visualiza. El segundo contiene todas las dependencias necesarias para el correcto funcionamiento de esta solución.

- el sub-directorio *data* contiene diferentes archivos *.csv* que son el origen de datos del modelo. Además contiene el modelo serializado y otros datos serializados que requiere la solución

- el sub-directorio *notebook* contiene el Jupyter notebook denominado **TP3-Clusterizacion.ipynb**

- el sub-directorio *resources* contiene recursos utilizados en este documento

- el sub-directorio *src* contiene contiene los sub-directorio *model* y *workers*. Además contiene la implementación de un interprete de línea de comandos y la web API para interactuar con el modelo

- el sub-directorio *model* contiene el código que implementa el modelo construido con el notebook **TP3-Clusterizacion.ipynb**

- el sub-directorio *workers* contiene los diferentes módulos encargados de prepocesar los datos y construir el modelo, entrenar el mismo y realizar predicciones.

### Ejecutando la solución

- Ejecutando el modelo implementado por código:

```

<ruta_a_dsa_model_deployment>/dsa-model-deployment/src/model/python clustering.py

```


- Ejecutando el interprete de línea de comandos:

```

<ruta_a_dsa_model_deployment>/dsa-model-deployment/src/python clustering_cli.py

```

Salida:

```

Usage: clustering_cli.py [OPTIONS] COMMAND [ARGS]...

Options:
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.

  --help                          Show this message and exit.

Commands:
  predict
  train
  train-predict

```

Ejemplo:

```

<ruta_a_dsa_model_deployment>/dsa-model-deployment/src/python clustering_cli.py train-predict 180 190.5 2

```

- Iniciando el servidor web

```

<ruta_a_dsa_model_deployment>/dsa-model-deployment/src/uvicorn server:app

````

Ejemplo de llamada al servidor web:

```
http://<host>:<port>/v1/train_predict

{
  "last_purchase_days": 10,
  "expense_avg": 500.5,
  "product_qty": 5
}

```

> *IMPORTANTE:* instalar la dependencias listadas en requirements.txt *antes* de ejecutar alguna de las funciones implementadas en esta solución. 
