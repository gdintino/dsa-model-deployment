import typer
import pickle
from workers import predictor, preprocessor, trainner

app = typer.Typer()


@app.command()
def train_predict(last_purchase_days: int, expense_avg: float, product_qty: int):
    print(u'Días desde la última compra: {}'.format(last_purchase_days))
    print(u'Monto promedio de compra: {}'.format(expense_avg))
    print(u'Cantidad de productos comprados: {}'.format(product_qty))
    print(u'Inicio train-predict')
    print(u'Pre-procesando...')
    try:
        df = preprocessor.preprocess()
        print(u'Entrenando...')
        trainner.train(df)
        print(u'Prediciendo...')
        prediction = predictor.predict(last_purchase_days, expense_avg, product_qty)
        print('Fin.')
    except Exception:
        raise Exception(u'Excepcion en comando train-predict {}'.format(Exception))
    print(u'Cliente tipo: {}'.format(prediction))

    return prediction


@app.command()
def train():
    try:
        print("Inicio train")
        df = preprocessor.preprocess()
        print('Datos procesados')
        print('Entrenando model')
        trainner.train(df)
        print('Fin train')
    except Exception:
        raise Exception(u'Excepcion en comando train {}'.format(Exception))

    return True

@app.command()
def predict(last_purchase_days: int, expense_avg: float, product_qty: int):
    print('Inicio predict')
    print(u'Días desde la última compra: {}'.format(last_purchase_days))
    print(u'Monto promedio de compra: {}'.format(expense_avg))
    print(u'Cantidad de productos comprados: {}'.format(product_qty))
    try:
        prediction = predictor.predict(last_purchase_days, expense_avg, product_qty)
    except Exception:
        raise Exception(u'Excepcion en comando predict {}'.format(Exception))
    print('Fin predict')
    print(u'Cliente del tipo: {}'.format(prediction))

    return prediction


if __name__ == "__main__":
    app()
