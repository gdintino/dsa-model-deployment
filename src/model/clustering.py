#Importing Libraries
#Basic libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

#Feature Selection
from sklearn.preprocessing import StandardScaler, MinMaxScaler, RobustScaler
#Modelling Algorithm
from sklearn.cluster import KMeans

data_dir = '../../data/'

#Load All The Data
olist_orders = pd.read_csv(data_dir + 'olist_orders_dataset.csv')
olist_products = pd.read_csv(data_dir + 'olist_products_dataset.csv')
olist_items = pd.read_csv(data_dir + 'olist_order_items_dataset.csv')
olist_customers = pd.read_csv(data_dir + 'olist_customers_dataset.csv')
olist_payments = pd.read_csv(data_dir + 'olist_order_payments_dataset.csv')
olist_sellers = pd.read_csv(data_dir + 'olist_sellers_dataset.csv')
olist_geolocation = pd.read_csv(data_dir + 'olist_geolocation_dataset.csv')
olist_reviews = pd.read_csv(data_dir + 'olist_order_reviews_dataset.csv')
olist_product_category_name = pd.read_csv(data_dir + 'product_category_name_translation.csv')

print(olist_product_category_name.head())

all_data = olist_orders.merge(olist_items, on='order_id', how='left')
all_data = all_data.merge(olist_payments, on='order_id', how='inner')
all_data = all_data.merge(olist_reviews, on='order_id', how='inner')
all_data = all_data.merge(olist_products, on='product_id', how='inner')
all_data = all_data.merge(olist_customers, on='customer_id', how='inner')
all_data = all_data.merge(olist_sellers, on='seller_id', how='inner')
all_data = all_data.merge(olist_product_category_name,on='product_category_name',how='inner')
#all_data = all_data.merge(olist_geolocation, on='seller_zip_code_prefix', how='inner')

#Vea qué porcentaje de datos está en blanco en cada columna
print(round((all_data.isnull().sum()/ len(all_data)*100),2))

# Viendo la información en los datos tanto el número de columnas, como la entrada a la memoria
print(all_data.info())

# PREPROCESAMIENTO DE DATOS

print(all_data['order_purchase_timestamp'].dtype)

# Cambie el tipo de datos en la columna de fecha para que el tipo de datos finalice
date_columns = ['order_purchase_timestamp', 'order_approved_at', 'order_delivered_carrier_date', 'order_delivered_customer_date',
             'order_estimated_delivery_date', 'shipping_limit_date', 'review_creation_date', 'review_answer_timestamp'] 
for col in date_columns:
    all_data[col] = pd.to_datetime(all_data[col], format='%Y-%m-%d %H:%M:%S')

# Ver si hay datos duplicados
print('Duplicados: ',all_data.duplicated().sum())

#Cree una columna de mes_orden para la exploración de datos
all_data['Month_order'] = all_data['order_purchase_timestamp'].dt.to_period('M').astype('str')

print(all_data[['Month_order','order_purchase_timestamp']].head())

# Elija entradas que van desde 01-2017 hasta 08-2018
#Porque hay datos que están fuera de balance con el promedio de cada mes en los datos antes del 01-2017 y después del 08-2018
# basado en datos de compra / order_purchase_timestamp
start_date = "2017-01-01"
end_date = "2018-08-31"

after_start_date = all_data['order_purchase_timestamp'] >= start_date
before_end_date = all_data['order_purchase_timestamp'] <= end_date
between_two_dates = after_start_date & before_end_date
all_data = all_data.loc[between_two_dates]

# Vea qué porcentaje de datos está en blanco en cada columna
print(round((all_data.isnull().sum()/ len(all_data)*100),2))

# Gestiona entradas vacías en la columna order_approved_at
missing_1 = all_data['order_approved_at'] - all_data['order_purchase_timestamp']
print(missing_1.describe())
print('='*50)
print('Mediana desde el momento en que se aprobó la orden: ',missing_1.median())

# tomamos la mediana porque hay quienes aprueban directamente desde el momento en que ordena, algunos son de hasta 60 días
add_1 = all_data[all_data['order_approved_at'].isnull()]['order_purchase_timestamp'] + missing_1.median()
all_data['order_approved_at'] = all_data['order_approved_at'].replace(np.nan, add_1)

# Gestiona entradas vacías en la columna order_approved_at
print(all_data[['order_purchase_timestamp', 'order_approved_at', 'order_delivered_carrier_date', 'order_delivered_customer_date']].head())

# Gestión de entradas vacías en la columna order_delivered_carrier_date
missing_2 = all_data['order_delivered_carrier_date'] - all_data['order_approved_at']
print(missing_2.describe())
print('='*50)
print('Mediana desde el momento de la solicitud hasta el envío: ',missing_2.median())

# Tomamos la mediana porque algunos barcos están dentro de las 21 horas del tiempo acordado, algunos hasta 107 días
add_2 = all_data[all_data['order_delivered_carrier_date'].isnull()]['order_approved_at'] + missing_2.median()
all_data['order_delivered_carrier_date']= all_data['order_delivered_carrier_date'].replace(np.nan, add_2)

# Gestión de entradas vacías en la columna order_delivered_customer_date
missing_3 = all_data['order_delivered_customer_date'] - all_data['order_delivered_carrier_date']
print(missing_3.describe())
print('='*50)
print('Mediana desde el momento en que se envió hasta que el cliente la recibió: ',missing_3.median())

# tomamos la mediana porque hay un tiempo de entrega de -17 días, lo que significa que es atípico, también hay un tiempo de entrega de hasta 205 días
add_3 = all_data[all_data['order_delivered_customer_date'].isnull()]['order_delivered_carrier_date'] + missing_3.median()
all_data['order_delivered_customer_date']= all_data['order_delivered_customer_date'].replace(np.nan, add_3)

# Manejar las columnas review_comment_title y review_comment_message
#Porque el número de entradas en blanco es muy grande e imposible de completar porque no hay variables que puedan
# usado para calcularlo. Porque este es el comentario y el título del comentario
# Luego eliminaremos la columna

all_data = all_data.drop(['review_comment_title', 'review_comment_message'], axis=1)

print(round((all_data.isna().sum()/ len(all_data)*100),3))

# Entrega de entrada vacía en las columnas product_weight_g, product_length_cm, product_height_cm, product_width_cm
#Porque solo hay 1, entonces lo dejamos caer
all_data = all_data.dropna()

# Compruebe si hay entradas en blanco
print(round((all_data.isnull().sum()/len(all_data)*100),4))

print(all_data[['order_item_id','product_name_lenght','product_description_lenght','product_photos_qty' ]].info())

# Ajuste el tipo de datos con los datos de entrada
all_data = all_data.astype({'order_item_id': 'int64', 
                            'product_name_lenght': 'int64',
                            'product_description_lenght':'int64', 
                            'product_photos_qty':'int64'})

# Extracción de características

#Cree una columna order_process_time para ver cuánto tiempo llevará iniciar el pedido hasta
# artículos son aceptados por los clientes
all_data['order_process_time'] = all_data['order_delivered_customer_date'] - all_data['order_purchase_timestamp']

#Cree una columna order_delivery_time para ver cuánto tiempo se requiere el tiempo de envío para cada pedido
all_data['order_delivery_time'] = all_data['order_delivered_customer_date'] - all_data['order_delivered_carrier_date']

#Cree una columna order_time_accuracy para ver si desde el tiempo estimado hasta que algo sea apropiado o tarde
# Si el valor es + positivo, entonces es más rápido hasta que, si es 0, está justo a tiempo, pero si es negativo, llega tarde
all_data['order_accuracy_time'] = all_data['order_estimated_delivery_date'] - all_data['order_delivered_customer_date'] 

#Cree una columna order_approved_time para ver cuánto tiempo tomará desde el pedido hasta la aprobación
all_data['order_approved_time'] = all_data['order_approved_at'] - all_data['order_purchase_timestamp'] 

#Cree una columna review_send_time para averiguar cuánto tiempo se envió la encuesta de satisfacción después de recibir el artículo.
all_data['review_send_time'] = all_data['review_creation_date'] - all_data['order_delivered_customer_date']

#Cree una columna review_answer_time para averiguar cuánto tiempo llevará completar una revisión después de
# envió una encuesta de satisfacción del cliente.
all_data['review_answer_time'] = all_data['review_answer_timestamp'] - all_data['review_creation_date']

# Combine las columnas product_length_cm, product_height_cm y product_width_cm para convertirlo en un volumen
# con una nueva columna, volumen_producto
all_data['product_volume'] = all_data['product_length_cm'] * all_data['product_height_cm'] * all_data['product_width_cm']

print(all_data['product_volume'].nunique())

print(all_data['order_process_time'].mean())

# ¿Qué productos tienen más demanda?
top_20_product_best_seller = all_data['order_item_id'].groupby(all_data['product_category_name_english']).sum().sort_values(ascending=False)[:20]
#print(top_20_product_best_seller)

# Lo trazamos para visualización
fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_20_product_best_seller.index,x=top_20_product_best_seller.values)
plt.title('Top 20 Most Selling Product',fontsize=20)
plt.xlabel('Total Product Sold',fontsize=17)
plt.ylabel('Product category',fontsize=17)

plt.show()

top_20_city_shopping = all_data['order_item_id'].groupby(all_data['customer_city']).sum().sort_values(ascending=False)[:20]
#print(top_20_city_shopping)

# ¿Qué ciudad compra más?
fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_20_city_shopping.index,x=top_20_city_shopping.values)
plt.title('Top 20 Most City Shopping',fontsize=20)
plt.xlabel('Total Product',fontsize=17)
plt.ylabel('City',fontsize=17)

plt.show()

# ¿Quién es el mayor número de compras de clientes en función de la cantidad de pedidos?
top_10_customer_shopping = all_data['order_item_id'].groupby(all_data['customer_id']).count().sort_values(ascending=False)[:10]
#print(top_10_customer_shopping)

# Lo trazamos para visualización
fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_10_customer_shopping.index,x=top_10_customer_shopping.values)
plt.title('Top 10 Customer Based on Order Amount',fontsize=20)
plt.xlabel('Amount of Product',fontsize=17)
plt.ylabel('Customer ID',fontsize=17)

plt.show()

# ¿Quién es el cliente con más gasto en compras por precio?
top_10_customer_shopping = all_data['payment_value'].groupby(all_data['customer_id']).sum().sort_values(ascending=False)[:10]
#print(top_10_customer_shopping)

fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_10_customer_shopping.index,x=top_10_customer_shopping.values)
plt.title('Top 10 Customer Based on Spending',fontsize=20)
plt.xlabel('Spending Amount',fontsize=17)
plt.ylabel('Customer ID',fontsize=17)

plt.show()

# ¿Qué vendedores venden más?
top_10_seller_order = all_data['order_item_id'].groupby(all_data['seller_id']).sum().sort_values(ascending=False)[:10]
#print(top_10_seller_order)

fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_10_seller_order.index,x=top_10_seller_order.values)
plt.title('Top 10 Seller Base on Sold Product',fontsize=20)
plt.xlabel('Total Product',fontsize=17)
plt.ylabel('Seller ID',fontsize=17)

plt.show()

# Vendedor, ¿cuál es el mayor ingreso basado en ingresos?
top_10_seller_order = all_data['price'].groupby(all_data['seller_id']).sum().sort_values(ascending=False)[:10]
#print(top_10_seller_order)

fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_10_seller_order.index,x=top_10_seller_order.values)
plt.title('Top 10 Seller Based on Revenue',fontsize=20)
plt.xlabel('Amount of Revenue',fontsize=17)
plt.ylabel('Seller ID',fontsize=17)

plt.show()

# Vendedor, ¿cuál es el mayor ingreso basado en ingresos?
top_10_seller_order = all_data[all_data['review_score'] == 5].groupby(all_data['seller_id']).sum().sort_values(by=['review_score'],ascending=False)[:10]
#print(top_10_seller_order)

fig=plt.figure(figsize=(16,9))
sns.barplot(y=top_10_seller_order.index,x=top_10_seller_order.review_score)
plt.title('Top 10 Seller Based on Review Score',fontsize=20)
plt.xlabel('Amount of Revenue',fontsize=17)
plt.ylabel('Seller ID',fontsize=17)

plt.show()

# Distribución del estado del pedido del cliente
print(round(all_data.order_status.value_counts() / len(all_data),2))

# ¿Cuál es el tiempo promedio desde el pedido hasta el recibo que se necesita en cada pedido mensual?
order_time_by_month = all_data['order_process_time'].groupby(all_data['Month_order']).median(numeric_only=False) #masukan argumen numeric_only untuk menghitung timedelta

fig=plt.figure(figsize=(16,9))
plt.plot(order_time_by_month.index, order_time_by_month.values, marker='o')
plt.title('Median Order Time By Month',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
          rotation=90)
plt.ylabel('Time (Day)',fontsize=17)

plt.show()

# ¿Cuál es el tiempo de entrega promedio requerido para cada primer pedido?
delivery_time_by_month = all_data['order_delivery_time'].groupby(all_data['Month_order']).median(numeric_only=False) #masukan argumen numeric_only untuk menghitung timedelta


fig=plt.figure(figsize=(16,9))
plt.plot(delivery_time_by_month.index, delivery_time_by_month.values / 86400, marker='o')
plt.title('Median Delivery Time By Month',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
          rotation=90)
plt.ylabel('Time (Day)',fontsize=17)

plt.show()

# ¿Cuál es la precisión media del tiempo de los envíos estimados y hasta el cliente en cada pedido mensual?
accuracy_time_by_month = all_data['order_accuracy_time'].groupby(all_data['Month_order']).median(numeric_only=False) #masukan argumen numeric_only untuk menghitung timedelta

fig=plt.figure(figsize=(16,9))
plt.plot(accuracy_time_by_month.index, accuracy_time_by_month.values / 86400, marker='o')
plt.title('Median Accuracy Time By Month',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
          rotation=90)
plt.ylabel('Time (Day)',fontsize=17)

plt.show()

# ¿Cuál es el período de tiempo promedio hasta que se aprueba desde el momento del pedido en cada pedido mensual?
approved_time_by_month = all_data['order_approved_time'].groupby(all_data['Month_order']).median(numeric_only=False) #masukan argumen numeric_only untuk menghitung timedelta

fig=plt.figure(figsize=(16,9))
plt.plot(approved_time_by_month.index, approved_time_by_month.values / 60, marker='o')
plt.title('Median Approved Time By Month',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(#[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
          rotation=90)
plt.ylabel('Time (Minutes)',fontsize=17)

plt.show()

# 10 categorías de productos con el tiempo más rápido desde el pedido hasta la aceptación del cliente
order_time_by_category = pd.DataFrame(all_data['order_process_time'].groupby(all_data['product_category_name_english']).median(numeric_only=False).sort_values(ascending=True)[:10])

fig=plt.figure(figsize=(16,9))
sns.barplot(y=order_time_by_category.index, x=order_time_by_category['order_process_time'].dt.days)
plt.title('Top 10 Fastest Product Category Order Time',fontsize=20)
plt.xlabel('Order Time (Day)',fontsize=17)
plt.ylabel('Product Category',fontsize=17)

plt.show()

# 10 categorías de productos con el mayor tiempo desde el pedido hasta la aceptación del cliente
order_time_by_category = pd.DataFrame(all_data['order_process_time'].groupby(all_data['product_category_name_english']).median(numeric_only=False).sort_values(ascending=False)[:10])

fig=plt.figure(figsize=(16,9))
sns.barplot(y=order_time_by_category.index, x=order_time_by_category['order_process_time'].dt.days)
plt.title('Top 10 Slowest Product Category Order Time',fontsize=20)
plt.xlabel('Order Time (Day)',fontsize=17)
plt.ylabel('Product Category',fontsize=17)

plt.show()

# ¿Cuánto cuesta el pedido cada mes?
order_count_by_month = all_data['order_item_id'].groupby(all_data['Month_order']).sum()

fig=plt.figure(figsize=(16,9))
sns.barplot(y=order_count_by_month.values, x=order_count_by_month.index, color="Salmon")
plt.title('Monthly Order',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(rotation=90)
plt.ylabel('Amount Order',fontsize=17)

plt.show()

# ¿Cuánto es el ingreso mensual?
revenue_count_by_month = all_data['payment_value'].groupby(all_data['Month_order']).sum()

fig=plt.figure(figsize=(16,9))
sns.barplot(y=revenue_count_by_month.values, x=revenue_count_by_month.index, color="Salmon")
plt.title('Monthly Revenue',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(rotation=90)
plt.ylabel('Amount Revenue',fontsize=17)

plt.show()

# ¿Cómo son los clientes activos cada mes?
customer_active_by_month = all_data.groupby('Month_order')['customer_unique_id'].nunique().reset_index()

fig=plt.figure(figsize=(16,9))
sns.barplot(y=customer_active_by_month['customer_unique_id'], x=customer_active_by_month['Month_order'], color="Salmon")
plt.title('Monthly Active User',fontsize=20)
plt.xlabel('Month',fontsize=17)
plt.xticks(rotation=90)
plt.ylabel('Amount of User',fontsize=17)

plt.show()

# Modelado
# Análisis RFM

print(all_data.info())

plt.hist(all_data['payment_sequential'])

plt.show()

plt.hist(all_data['review_score'])

plt.show()

##Nos quedamos con los días que tardaron, respecto de los que estimaban. 
## Positivo implica cuántos días antes llega de lo que se esperaba, y negativo cuantos días de retraso. 

all_data['order_accuracy_time'] = all_data['order_accuracy_time'].dt.days.astype('int64')

plt.boxplot(all_data['order_accuracy_time'])

plt.show()

print(all_data['order_accuracy_time'].describe())

##Veamos cuántos datos en promedio hay por encima del tercer cuartil y por debajo del primer cuartil 
print("mayor al segundo cuartil en promedio",
      (sum(all_data['order_accuracy_time']>all_data['order_accuracy_time'].quantile(0.75)))/116191)
print("menor al primer cuartil en promedio",
      (sum(all_data['order_accuracy_time']<all_data['order_accuracy_time'].quantile(0.25)))/116191)

# Vemos que hay demasiados valores fuera de los cuartiles, así que vamos a desestimar la información
##Creamos también la variable que nos dice el tiempo que pasó en días desde la última compra del cliente
Days_from_purchase = (max(all_data['order_purchase_timestamp']))-all_data['order_purchase_timestamp']
Days_from_purchase = Days_from_purchase.dt.days.astype('int64')
all_data['Days_from_purchase'] = Days_from_purchase

### Veamos cual de los dos tiene menos registros, entre 'customer_id' y 'customer_unique_id'
numero = np.unique(all_data['customer_id'])
print ('la cantidad de registros unicos de customer_id es: ',numero.shape)
numero2 = np.unique(all_data['customer_unique_id'])
print ('la cantidad de registros unicos de customer_unique_id es: ',numero2.shape)
##Vemos que 'customer_unique_id' es el ganador, así que vamos a trabajar nuestras varibles sobre el 
Dias_p_cliente = all_data[['customer_unique_id', 'Days_from_purchase']].copy()

mmr =Dias_p_cliente['Days_from_purchase'].groupby(Dias_p_cliente['customer_unique_id']).min()

df= pd.DataFrame(mmr)
df['customer_unique_id']=df.index
df.drop(columns=['customer_unique_id'], inplace=True)
df=df.rename(columns={'Days_from_purchase':'Days_from_last_purchase'}) 
print(df['Days_from_last_purchase'])

### Armaremos una columna del df que contenga el promedio que gastó cada cliente
mr =all_data['payment_value'].groupby(all_data['customer_unique_id']).mean()
df['mean_payment']=mr
print(df.head())

## Agregamos una columna de la cantidad de productos comprados en todo el periodo
cant_prod_comp =all_data['order_item_id'].groupby(all_data['customer_unique_id']).sum()
df['cant_prod_comp'] = cant_prod_comp

### Agregamos una columna del volumen promedio del producto adquirido por cliente
vol_prom =all_data['product_volume'].groupby(all_data['customer_unique_id']).mean()
df['product_volume_mean'] = vol_prom

### Agregamos una columna de la cantidad promedio de cuotas en las que compra cada cliente
cant_prom_cuotas =all_data['payment_installments'].groupby(all_data['customer_unique_id']).mean()
df['cant_prom_cuotas'] = cant_prom_cuotas

### Agregamos una columna de la puntuación promedio que le da cada cliente a los vendedor
punt_prom_a_vend =all_data['review_score'].groupby(all_data['customer_unique_id']).mean()
df['punt_prom_a_vend'] = punt_prom_a_vend

### Agregamos una columna del gasto promedio del envio por cliente.
valor_prom_envio =all_data['freight_value'].groupby(all_data['customer_unique_id']).mean()
df['valor_prom_envio'] = valor_prom_envio

print(df)

# Desde acá empieza el escalado y modelado usando K-means
X = np.array(df[["Days_from_last_purchase", "mean_payment", "cant_prod_comp"]])

## Escalaremos nuestros datos
sc = StandardScaler()
X_std = sc.fit_transform(X)

print('SC fit transfom')
print(X_std)

### Esta parte tardó como media hora en mi computadora.
from sklearn.metrics import silhouette_score

sil = []
Nc = range(2, 21)
for k in range(2, 21):
  kmeans = KMeans(n_clusters = k).fit(X_std)
  labels = kmeans.labels_
  sil.append(silhouette_score(X_std, labels, metric = 'euclidean'))

print(sil)

plt.plot(Nc,sil)
plt.xlabel('Cantidad de clusters',color = 'w')
plt.ylabel('Score',color = 'w')
plt.title('Silhouette Curve (Curva silueta)',color='y')
plt.xticks(Nc,color='g')
plt.yticks(color='g')
plt.show()

kmeans = KMeans(n_clusters=5).fit(X_std)
centroids = kmeans.cluster_centers_
print(centroids)

from mpl_toolkits.mplot3d import Axes3D
# Obtenemos las etiquetas de cada punto de nuestros datos
labels = kmeans.predict(X_std)
# Obtenemos los centroids
C = kmeans.cluster_centers_
colores=['red','green','blue','yellow','brown']
clientes=['Cliente_1','Cliente_2','Cliente_3','Cliente_4','Cliente_5']
clasif= []
asignar=[]
for row in labels:
    asignar.append(colores[row])
    clasif.append(clientes[row])
fig = plt.figure(figsize=(10,8))
ax = Axes3D(fig)
ax.scatter(X_std[:, 0], X_std[:, 1], X_std[:, 2],c=asignar,s=60)
ax.scatter(C[:, 0], C[:, 1], C[:, 2], marker='*', c=colores, s=1000)
clasificacion = clasif

plt.show()

# Descripción de los clusters
## Descripción de los centroides
centroides = sc.inverse_transform(centroids)
centroidesdf = pd.DataFrame(centroides, columns = ["Dias_desde_ult_comp", "Gasto_prom", "cant_prod_comp"],
                            index = ['Cliente_1','Cliente_2','Cliente_3','Cliente_4','Cliente_5'])
print(centroidesdf)

dataframe = pd.DataFrame(X,columns = ["Dias_desde_ult_comp", "Gasto_prom", "cant_prod_comp"])
dataframe['clasificacion']= clasificacion
#dataframe.head(30)
fig = plt.figure(figsize=(30,10))
plt.subplot2grid((2,3),(0,0))
dataframe['clasificacion'].value_counts().plot(kind='bar',title = 'Cantidad en cada cluster')

plt.subplot2grid((2,3),(0,1))
dataframe['clasificacion'].value_counts().plot(kind= 'pie',title = 'Cantidad en cada cluster')

plt.show()

print(dataframe['clasificacion'].value_counts())

### Para clasificar un nuevo cliente, deberemos pasarle un array de tres valores
import numpy as np
Dia_desde_ult_comp_NC = float(input('¿Hace cuanto compro el último producto?(en días)')) 
Gasto_prom_NC = float(input('¿Cuánto gastó en promedio?'))
Cant_prod_comp_NC = float(input('¿Cuántos productos compró?'))
Nuevo_cliente = np.array([Dia_desde_ult_comp_NC, Gasto_prom_NC, Cant_prod_comp_NC])
Nuevo_cliente_Std = sc.transform([Nuevo_cliente])
labels_NC = kmeans.predict(Nuevo_cliente_Std)
if labels_NC == 0: print ('El nuevo cliente es de tipo 1')
elif labels_NC == 1: print ('El nuevo cliente es de tipo 2')
elif labels_NC == 2: print ('El nuevo cliente es de tipo 3')
elif labels_NC == 3: print ('El nuevo cliente es de tipo 4')
elif labels_NC == 4: print ('El nuevo cliente es de tipo 5')
