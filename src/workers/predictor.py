import pickle
import numpy as np


def predict(last_purchase_days, expense_avg, product_qty):
    with open('../data/clustering.model', 'rb') as c:
        try:
            model = pickle.load(c)
        except Exception:
            raise Exception('clustering.model no encontrado')
    with open('../data/means.dat', 'rb') as m:
        try:
            means = pickle.load(m)
        except Exception:
            raise Exception('means.dat no encontrado')
    with open('../data/stds.dat', 'rb') as s:
        try:
            stds = pickle.load(s)
        except Exception:
            raise Exception('stds.dat no encontrado')
    new_client = np.array([[last_purchase_days, expense_avg, product_qty]])
    casiZ = np.array(new_client - means)
    new_client_std = np.array([casiZ[0, 0] / stds[0], casiZ[0, 1] / stds[1], casiZ[0, 2] / stds[2]], ndmin=2)
    labels_NC = model.predict(new_client_std)

    return '{}'.format(labels_NC)
