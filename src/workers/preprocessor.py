import pandas as pd
import numpy as np

pd.options.mode.chained_assignment = None  # default='warn'


def preprocess():
    data_dir = '../data/'
    # Load All The Data
    olist_orders = pd.read_csv(data_dir + 'olist_orders_dataset.csv')
    olist_products = pd.read_csv(data_dir + 'olist_products_dataset.csv')
    olist_items = pd.read_csv(data_dir + 'olist_order_items_dataset.csv')
    olist_customers = pd.read_csv(data_dir + 'olist_customers_dataset.csv')
    olist_payments = pd.read_csv(data_dir + 'olist_order_payments_dataset.csv')
    olist_sellers = pd.read_csv(data_dir + 'olist_sellers_dataset.csv')
    # olist_geolocation = pd.read_csv(data_dir + 'olist_geolocation_dataset.csv')
    olist_reviews = pd.read_csv(data_dir + 'olist_order_reviews_dataset.csv')
    olist_product_category_name = pd.read_csv(data_dir + 'product_category_name_translation.csv')

    all_data = olist_orders.merge(olist_items, on='order_id', how='left')
    all_data = all_data.merge(olist_payments, on='order_id', how='inner')
    all_data = all_data.merge(olist_reviews, on='order_id', how='inner')
    all_data = all_data.merge(olist_products, on='product_id', how='inner')
    all_data = all_data.merge(olist_customers, on='customer_id', how='inner')
    all_data = all_data.merge(olist_sellers, on='seller_id', how='inner')
    all_data = all_data.merge(olist_product_category_name, on='product_category_name', how='inner')
    # all_data = all_data.merge(olist_geolocation, on='seller_zip_code_prefix', how='inner')

    model_data = all_data

    # Cambie el tipo de datos en la columna de fecha para que el tipo de datos finalice
    date_columns = ['order_purchase_timestamp', 'order_approved_at', 'order_delivered_carrier_date',
                    'order_delivered_customer_date',
                    'order_estimated_delivery_date', 'shipping_limit_date', 'review_creation_date',
                    'review_answer_timestamp']
    for col in date_columns:
        model_data[col] = pd.to_datetime(model_data[col], format='%Y-%m-%d %H:%M:%S')

    # Cree una columna de mes_orden para la exploración de datos
    model_data['Month_order'] = model_data['order_purchase_timestamp'].dt.to_period('M').astype('str')

    # Elija entradas que van desde 01-2017 hasta 08-2018
    # Porque hay datos que están fuera de balance con el promedio de cada mes en los datos antes del 01-2017 y después del 08-2018
    # basado en datos de compra / order_purchase_timestamp
    start_date = "2017-01-01"
    end_date = "2018-08-31"

    after_start_date = model_data['order_purchase_timestamp'] >= start_date
    before_end_date = model_data['order_purchase_timestamp'] <= end_date
    between_two_dates = after_start_date & before_end_date
    model_data = model_data.loc[between_two_dates]

    # Gestiona entradas vacías en la columna order_approved_at
    missing_1 = model_data['order_approved_at'] - model_data['order_purchase_timestamp']

    # tomamos la mediana porque hay quienes aprueban directamente desde el momento en que ordena, algunos son de hasta 60 días
    add_1 = model_data[model_data['order_approved_at'].isnull()]['order_purchase_timestamp'] + missing_1.median()
    model_data['order_approved_at'] = model_data['order_approved_at'].replace(np.nan, add_1)

    # Gestión de entradas vacías en la columna order_delivered_carrier_date
    missing_2 = model_data['order_delivered_carrier_date'] - model_data['order_approved_at']

    # Tomamos la mediana porque algunos barcos están dentro de las 21 horas del tiempo acordado, algunos hasta 107 días
    add_2 = model_data[model_data['order_delivered_carrier_date'].isnull()]['order_approved_at'] + missing_2.median()
    model_data['order_delivered_carrier_date'] = model_data['order_delivered_carrier_date'].replace(np.nan, add_2)

    # Gestión de entradas vacías en la columna order_delivered_customer_date
    missing_3 = model_data['order_delivered_customer_date'] - model_data['order_delivered_carrier_date']

    # tomamos la mediana porque hay un tiempo de entrega de -17 días, lo que significa que es atípico, también hay un tiempo de entrega de hasta 205 días
    add_3 = model_data[model_data['order_delivered_customer_date'].isnull()][
                'order_delivered_carrier_date'] + missing_3.median()
    model_data['order_delivered_customer_date'] = model_data['order_delivered_customer_date'].replace(np.nan, add_3)

    # Manejar las columnas review_comment_title y review_comment_message
    # Porque el número de entradas en blanco es muy grande e imposible de completar porque no hay variables que puedan
    # usado para calcularlo. Porque este es el comentario y el título del comentario
    # Luego eliminaremos la columna

    model_data = model_data.drop(['review_comment_title', 'review_comment_message'], axis=1)

    # Entrega de entrada vacía en las columnas product_weight_g, product_length_cm, product_height_cm, product_width_cm
    # Porque solo hay 1, entonces lo dejamos caer
    model_data = model_data.dropna()

    # Ajuste el tipo de datos con los datos de entrada
    model_data = model_data.astype({'order_item_id': 'int64',
                                    'product_name_lenght': 'int64',
                                    'product_description_lenght': 'int64',
                                    'product_photos_qty': 'int64'})

    # Extracción de características

    # Cree una columna order_process_time para ver cuánto tiempo llevará iniciar el pedido hasta
    # artículos son aceptados por los clientes
    model_data['order_process_time'] = model_data['order_delivered_customer_date'] - model_data[
        'order_purchase_timestamp']

    # Cree una columna order_delivery_time para ver cuánto tiempo se requiere el tiempo de envío para cada pedido
    model_data['order_delivery_time'] = model_data['order_delivered_customer_date'] - model_data[
        'order_delivered_carrier_date']

    # Cree una columna order_time_accuracy para ver si desde el tiempo estimado hasta que algo sea apropiado o tarde
    # Si el valor es + positivo, entonces es más rápido hasta que, si es 0, está justo a tiempo, pero si es negativo, llega tarde
    model_data['order_accuracy_time'] = model_data['order_estimated_delivery_date'] - model_data[
        'order_delivered_customer_date']

    # Cree una columna order_approved_time para ver cuánto tiempo tomará desde el pedido hasta la aprobación
    model_data['order_approved_time'] = model_data['order_approved_at'] - model_data['order_purchase_timestamp']

    # Cree una columna review_send_time para averiguar cuánto tiempo se envió la encuesta de satisfacción después de recibir el artículo.
    model_data['review_send_time'] = model_data['review_creation_date'] - model_data['order_delivered_customer_date']

    # Cree una columna review_answer_time para averiguar cuánto tiempo llevará completar una revisión después de
    # envió una encuesta de satisfacción del cliente.
    model_data['review_answer_time'] = model_data['review_answer_timestamp'] - model_data['review_creation_date']

    # Combine las columnas product_length_cm, product_height_cm y product_width_cm para convertirlo en un volumen
    # con una nueva columna, volumen_producto
    model_data['product_volume'] = model_data['product_length_cm'] * model_data['product_height_cm'] * model_data[
        'product_width_cm']

    # Modelado
    # Análisis RFM

    # Nos quedamos con los días que tardaron, respecto de los que estimaban.
    # Positivo implica cuántos días antes llega de lo que se esperaba, y negativo cuantos días de retraso.

    model_data['order_accuracy_time'] = model_data['order_accuracy_time'].dt.days.astype('int64')

    # Vemos que hay demasiados valores fuera de los cuartiles, así que vamos a desestimar la información
    # Creamos también la variable que nos dice el tiempo que pasó en días desde la última compra del cliente
    Days_from_purchase = (max(model_data['order_purchase_timestamp'])) - model_data['order_purchase_timestamp']
    Days_from_purchase = Days_from_purchase.dt.days.astype('int64')
    model_data['Days_from_purchase'] = Days_from_purchase

    Dias_p_cliente = model_data[['customer_unique_id', 'Days_from_purchase']].copy()

    mmr = Dias_p_cliente['Days_from_purchase'].groupby(Dias_p_cliente['customer_unique_id']).min()

    df = pd.DataFrame(mmr)
    df['customer_unique_id'] = df.index
    df.drop(columns=['customer_unique_id'], inplace=True)
    df = df.rename(columns={'Days_from_purchase': 'Days_from_last_purchase'})

    # Armaremos una columna del df que contenga el promedio que gastó cada cliente
    mr = model_data['payment_value'].groupby(model_data['customer_unique_id']).mean()
    df['mean_payment'] = mr

    # Agregamos una columna de la cantidad de productos comprados en todo el periodo
    cant_prod_comp = model_data['order_item_id'].groupby(model_data['customer_unique_id']).sum()
    df['cant_prod_comp'] = cant_prod_comp

    # Agregamos una columna del volumen promedio del producto adquirido por cliente
    vol_prom = model_data['product_volume'].groupby(model_data['customer_unique_id']).mean()
    df['product_volume_mean'] = vol_prom

    # Agregamos una columna de la cantidad promedio de cuotas en las que compra cada cliente
    cant_prom_cuotas = model_data['payment_installments'].groupby(model_data['customer_unique_id']).mean()
    df['cant_prom_cuotas'] = cant_prom_cuotas

    # Agregamos una columna de la puntuación promedio que le da cada cliente a los vendedor
    punt_prom_a_vend = model_data['review_score'].groupby(model_data['customer_unique_id']).mean()
    df['punt_prom_a_vend'] = punt_prom_a_vend

    # Agregamos una columna del gasto promedio del envio por cliente.
    valor_prom_envio = model_data['freight_value'].groupby(model_data['customer_unique_id']).mean()
    df['valor_prom_envio'] = valor_prom_envio

    return df
