from sklearn.preprocessing import StandardScaler
import numpy as np
from sklearn.cluster import KMeans
import pickle


def train(df):
    X = np.array(df[["Days_from_last_purchase", "mean_payment", "cant_prod_comp"]])
    sc = StandardScaler()
    X_std = sc.fit_transform(X)
    model_trained = KMeans(n_clusters=5).fit(X_std)
    means = np.array((X[:, 0].mean(), X[:, 1].mean(), X[:, 2].mean()))
    stds = np.array((X[:, 0].std(), X[:, 1].std(), X[:, 2].std()))
    with open('../data/clustering.model', 'wb') as f:
        pickle.dump(model_trained, f)
    with open('../data/means.dat', 'wb') as m:
        pickle.dump(means, m)
    with open('../data/stds.dat', 'wb') as s:
        pickle.dump(stds, s)

    return model_trained
