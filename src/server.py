from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional
from clustering_cli import train_predict, predict, train

app = FastAPI()


class Payload(BaseModel):
    last_purchase_days: int
    expense_avg: float
    product_qty: int


class TrainPayload(BaseModel):
    data_dir: Optional[str] = None
    model_file: Optional[str] = None


@app.post("/v1/train_predict")
async def run_train_predict(payload: Payload):
    prediction = train_predict(payload.last_purchase_days, payload.expense_avg, payload.product_qty)

    return {"Cliente tipo": prediction}


@app.post("/v1/predict")
async def run_predict(payload: Payload):
    prediction = predict(payload.last_purchase_days, payload.expense_avg, payload.product_qty)

    return {"Cliente tipo": prediction}


@app.post("/v1/train")
async def run_train():
    r = train()

    return {"Modelo entrenado": r}
